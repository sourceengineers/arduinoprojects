//Pins definieren
int LEDrot = 3, LEDgruen = 4, LEDgelb = 5, LEDweiss = 6, Button = 8;

//Status der LEDs
int StateRot, StateGruen, StateGelb, StateWeiss;


//Blinkdauer
const int Blinkdauer = 500;


// Vier Funktionen um LEDs blinken zu lassen
void rot(unsigned long *t, unsigned long *t0){
  *t = millis();
  if(*t-*t0 >= Blinkdauer){
    *t0 = *t;
    if(StateRot == LOW){
      StateRot = HIGH;
    }
    else{
      StateRot = LOW;
    }
    digitalWrite(LEDrot, StateRot);
  }
}

void gruen(unsigned long *t, unsigned long *t0){
  *t = millis();
  if(*t-*t0 >= Blinkdauer){
    *t0 = *t;
    if(StateGruen == LOW){
      StateGruen = HIGH;
    }
    else{
      StateGruen = LOW;
    }
    digitalWrite(LEDgruen, StateGruen);
  }
}
void gelb(unsigned long *t, unsigned long *t0){
  *t = millis();
  if(*t-*t0 >= Blinkdauer){
    *t0 = *t;
    if(StateGelb == LOW){
      StateGelb = HIGH;
    }
    else{
      StateGelb = LOW;
    }
    digitalWrite(LEDgelb, StateGelb);
  }
}
void weiss(unsigned long *t, unsigned long *t0){
  *t = millis();
  if(*t-*t0 >= Blinkdauer){
    *t0 = *t;
    if(StateWeiss == LOW){
      StateWeiss = HIGH;
    }
    else{
      StateWeiss = LOW;
    }
    digitalWrite(LEDweiss, StateWeiss);
  }
}

bool Knopf(){
  static bool Gedrueckt = false;
  if((digitalRead(Button) == HIGH) && (Gedrueckt == false)){
    Gedrueckt = true;
    delay(30);
    return true;
  }
  else{
    if(digitalRead(Button) == LOW){
      Gedrueckt = false;
      return false;
    }
    else{
      return false;
    }
  }
}

//Funktionspointer definieren
void(*Funktionspointer)(unsigned long *, unsigned long *);

//Array mit Funktionsadressen
int Adressen[] = {rot, gruen, gelb, weiss};

//Pointer aufs Array
int Startadresse = &Adressen[0];
int Endadresse = &Adressen[3];
int *Arraypointer;

void setup() {
  pinMode(LEDrot, OUTPUT);
  pinMode(LEDgruen, OUTPUT);
  pinMode(LEDgelb, OUTPUT);
  pinMode(LEDweiss, OUTPUT);
  pinMode(Button, INPUT);

  StateRot = LOW;

  Serial.begin(9600);

  Arraypointer = Startadresse;
  Funktionspointer = rot;

}

void loop() {
  //Blinkzeiten
  unsigned long t, t0;
  bool KnopfStatus = false;
  //Pointer auf Zeitvariable
  unsigned long *Zeit;
  unsigned long *Anfangszeit;
  Zeit = &t;
  Anfangszeit = &t0;
  
  int a = Funktionspointer;
  Serial.println(a);
  Serial.println("*****");
  Funktionspointer(Zeit, Anfangszeit);
  KnopfStatus = Knopf();
  if(KnopfStatus == 1){
    digitalWrite(LEDrot, LOW);
    digitalWrite(LEDgruen, LOW);
    digitalWrite(LEDgelb, LOW);
    digitalWrite(LEDweiss, LOW);
    
    if(Arraypointer < Endadresse){
      Arraypointer++;
    }
    else{
      Arraypointer = Startadresse;
    }
    Funktionspointer = *Arraypointer;
  }
}
