#include <Adafruit_NeoPixel.h>
#define LED 13 //definiert den Pin vom Ring
#define N 6 //definiert Anzahl LEDs für den Ring (Mittleres weggelassen

Adafruit_NeoPixel strip(N, LED, NEO_GRB + NEO_KHZ800);

const int intervall=500;  //legt Wechselintervall fest
unsigned long t=0;  //Zeit für Messung
unsigned long t0=0; //Zeit der letzten Aktion

int Zustand=0;  //Definiert den Zustand für [Statemachine 1]
int Uhrzeigersinn=0;  //definiert die Drehrichtung

void Wechsel(){ //definieft die Funktion zum Wechseln des LEDs (im Kreis) [Statemachine 1]
  if(Uhrzeigersinn==0){ //Drehen im Gegenuhrzeigersinn
    switch(Zustand){
      case 0:
        strip.clear();
        strip.setPixelColor(1, 255, 0, 255);
        strip.show();
        Zustand=Zustand+1;
        break;
      case 1:
        strip.clear();
        strip.setPixelColor(2, 255, 0, 255);
        strip.show();
        Zustand=Zustand+1;
        break;
      case 2:
        strip.clear();
        strip.setPixelColor(3, 255, 0, 255);
        strip.show();
        Zustand=Zustand+1;
        break;
      case 3:
        strip.clear();
        strip.setPixelColor(4, 255, 0, 255);
        strip.show();
        Zustand=Zustand+1;
        break;
      case 4:
        strip.clear();
        strip.setPixelColor(5, 255, 0, 255);
        strip.show();
        Zustand=Zustand+1;
        break;
      case 5:
        strip.clear();
        strip.setPixelColor(0, 255, 0, 255);
        strip.show();
        Zustand=0;
        break;
    }
  }
  if(Uhrzeigersinn==1){ //Drehen im Uhrzeigersinn
    switch(Zustand){
      case 0:
        strip.clear();
        strip.setPixelColor(1, 255, 0, 255);
        strip.show();
        Zustand=5;
        break;
      case 1:
        strip.clear();
        strip.setPixelColor(2, 255, 0, 255);
        strip.show();
        Zustand=0;
        break;
      case 2:
        strip.clear();
        strip.setPixelColor(3, 255, 0, 255);
        strip.show();
        Zustand=1;
        break;
      case 3:
        strip.clear();
        strip.setPixelColor(4, 255, 0, 255);
        strip.show();
        Zustand=2;
        break;
      case 4:
        strip.clear();
        strip.setPixelColor(5, 255, 0, 255);
        strip.show();
        Zustand=3;
        break;
      case 5:
        strip.clear();
        strip.setPixelColor(0, 255, 0, 255);
        strip.show();
        Zustand=4;
        break;
    }
  }
}


void setup() {
  pinMode(LED,OUTPUT);
  strip.begin();  //Startet den Ring
  strip.show(); //Schaltet alle LEDs aus
  Serial.begin(9600);

}

void loop() {
  t=millis(); //Zeit seit Programmstart aufrufen
  if(t-t0>=intervall){
    Wechsel();
    t0=t;
    Serial.print(Zustand);
  }

}
