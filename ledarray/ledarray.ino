int Array[7]; //definiert das Array (Speicher)

int z=0;  //Zähler für die Aufnahme im Array
int a=0;  //Zähler für Durchläufe

bool warten=false;  //Sperrt die Köpfe (verhindert mehrfachauslösung bei 1x Drücken
int state=0;  //Merkt sich welcher Knopf gedrückt wurde

int ms=50;  //Delay fürs Debouncen
int blinkdauer=200; //Delay fürs Blinken

void Aufzeichnen(){ //Diese Funktion zeichnet die Tastenfolge auf und speichert sie im Delay
  if(warten==false){  //Abfragen, ob er warten muss
    for(a=0; a<7; a++){ //Alle Schalterpins (0-6) abfragen ob High
      if(digitalRead(a)==HIGH){ //Wenn einer High
        Serial.print("Down"); //Ausgeben, dass Schalter unten
        delay(ms);  //Warten (debouncing)
        Array[z]=a; //Pinnummer im Array speichern
        Serial.println(Array[0]); //Array Ausgeben
        Serial.println(Array[1]);
        Serial.println(Array[2]);
        Serial.println(Array[3]);
        Serial.println(Array[4]);
        Serial.println(Array[5]);
        Serial.println(Array[6]);
        Serial.println("****");
        z++;  //eine Stelle weiter im Array
        warten=true;  //Warten Aktivieren (verhindert mehrfaches Drücken)
        break;  //Schleife beenden
      }
    }
  }
  else{
    if(digitalRead(a)==LOW){  //Schauen ob der vorher gedrückte pin wieder losgelassen ist
      Serial.println("up"); //Ausgeben, dass Schalter oben ist
      delay(ms);
      warten=false; //Warten abschliessen
    }
  }
}

void Abspielen(){ //Funktion fürs Abspielen des Arrays
  for(int c=0; c<7; c++){ //Array in Schleife auslesen und jeweils LED blinken lassen
    digitalWrite((Array[c]+7),HIGH);  //Entsprechendes LED ist immer 7 Pins weiter als Schalter
    delay(blinkdauer);
    digitalWrite((Array[c]+7),LOW);
    delay(blinkdauer);
  }
}


void setup() {
  Serial.begin(9600); //Serielle Schnittstelle aktivieren
  
  int e=0;
  while(e<7){ //Alle Pins von 0-6 input (Schalter)
    pinMode(e,INPUT);
    e++;
  }
  int i=7;
  while(i<14){  //Alle Pins von 7-13 output (LEDs)
    pinMode(i,OUTPUT);
    i++;
  }
}

void loop() {
  while(z<7){Aufzeichnen();}  //Aufzeichnen, bis das Array voll ist
  Abspielen();  //Abspielen
  z=0;  //Z wieder zurücksetzen (von neuem starten)
}
