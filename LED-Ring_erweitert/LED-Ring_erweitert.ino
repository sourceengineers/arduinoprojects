#include <Adafruit_NeoPixel.h>
#define LED 13 //definiert den Pin vom Ring
#define N 6 //definiert Anzahl LEDs für den Ring (Mittleres weggelassen

#define KNOPF 6 //definiert den Pin des Knopfs

Adafruit_NeoPixel strip(N, LED, NEO_GRB + NEO_KHZ800);  //Konfiguriert den Ring

//Zeiten fürs Drehen und leuchten
const int intervall=100;  //legt Wechselintervall fest
const int leuchtintervall=5000; //legt Leuchtintervall fest
unsigned long t=0;  //Zeit für Messung
unsigned long t0=0; //Zeit der letzten Aktion

//Zustände fürs Drehen
int Zustand1=0;  //Definiert den Zustand für [Statemachine 1]
int Uhrzeigersinn=1;  //definiert die Drehrichtung

//Zustände fürs Lesen
int Zustand2=0;

//Zeiten und Zustände für den Knopf
const int lange=2000; //Zeit für langes Drücken
const int laange=4000; //Zeit für sehr langes Drücken
unsigned long z=0;  //Zeit für Messung
unsigned long z0=0; //Zeit des letzten Drückens
unsigned long z1=0; //Zeit fürs debouncing
int debounce=20;
int push=0;

//Werte für Farben
long a=11;
long b=123;
long c=64;


void Wechsel(){ //definieft die Funktion zum Wechseln des LEDs (im Kreis) [Statemachine 1]
  if(Uhrzeigersinn==0){ //Drehen im Gegenuhrzeigersinn
    switch(Zustand1){
      case 0:
        strip.clear();
        strip.setPixelColor(1, a, b, c);
        strip.show();
        Zustand1=Zustand1+1;
        break;
      case 1:
        strip.clear();
        strip.setPixelColor(2, a, b, c);
        strip.show();
        Zustand1=Zustand1+1;
        break;
      case 2:
        strip.clear();
        strip.setPixelColor(3, a, b, c);
        strip.show();
        Zustand1=Zustand1+1;
        break;
      case 3:
        strip.clear();
        strip.setPixelColor(4, a, b, c);
        strip.show();
        Zustand1=Zustand1+1;
        break;
      case 4:
        strip.clear();
        strip.setPixelColor(5, a, b, c);
        strip.show();
        Zustand1=Zustand1+1;
        break;
      case 5:
        strip.clear();
        strip.setPixelColor(0, a, b, c);
        strip.show();
        Zustand1=0;
        break;
    }
  }
  if(Uhrzeigersinn==1){ //Drehen im Uhrzeigersinn
    switch(Zustand1){
      case 0:
        strip.clear();
        strip.setPixelColor(1, a, b, c);
        strip.show();
        Zustand1=5;
        break;
      case 1:
        strip.clear();
        strip.setPixelColor(2, a, b, c);
        strip.show();
        Zustand1=0;
        break;
      case 2:
        strip.clear();
        strip.setPixelColor(3, 255, b, c);
        strip.show();
        Zustand1=1;
        break;
      case 3:
        strip.clear();
        strip.setPixelColor(4, a, b, c);
        strip.show();
        Zustand1=2;
        break;
      case 4:
        strip.clear();
        strip.setPixelColor(5, a, b, c);
        strip.show();
        Zustand1=3;
        break;
      case 5:
        strip.clear();
        strip.setPixelColor(0, a, b, c);
        strip.show();
        Zustand1=4;
        break;
    }
  }
}

void Drehen(){
  t=millis(); //Zeit seit Programmstart aufrufen
  if(t-t0>=intervall){  //Wenn Intervall erreicht Funktion Wechsel aufrufen
    Wechsel();
    t0=t; //Counter angleichen
  }
}

void Leuchten(){
  strip.clear();
  strip.setPixelColor(0, a, b, c);
  strip.setPixelColor(1, a, b, c);
  strip.setPixelColor(2, a, b, c);
  strip.setPixelColor(3, a, b, c);
  strip.setPixelColor(4, a, b, c);
  strip.setPixelColor(5, a, b, c);
  strip.show();
}

void Lesen(){ //Übergeordnete Statemachine fürs Auslesen des Knopfs [Statemachine 2]
  switch(Zustand2){
    case 0: //Anfangszustand, LEDs drehen und warten bis Knopf gedrückt wird
      Drehen();
      push=digitalRead(KNOPF);
      if(push==HIGH){
        Zustand2=1;
        z1=millis();
      }
      break;
    case 1: //Knopf wurde gedrückt, Zeit Zählen bis losgelassen
      Drehen();
      z=millis(); //Zeit stoppen für Drückdauer
      if(z-z1>=debounce){
        push=digitalRead(KNOPF);
        if(push==LOW){
          z0=millis();
          if(z0-z1>=lange && z0-z1<laange){ 
            Zustand2=3;
          }
          else if(z0-z1>=laange){
            Zustand2=4;
          }
          else{
            Zustand2=2;
          }
        }
      }
      break;
    case 2: //Knopf wurde kurz gedrückt, Drehrichtung ändern
      Uhrzeigersinn=1-Uhrzeigersinn;
      Zustand2=0;
      break;
    case 3: //Knopf wurde lange (2-4s) gedrückt, leuchten
      Leuchten();
      delay(5000);
      Zustand2=0;
      break;
    case 4: //Knopf mehr als 4s gedrückt Farbe wechseln
      a=random(255);
      b=random(255);
      c=random(255);
      Zustand2=0;
      break;
  }
}

void setup() {
  pinMode(LED,OUTPUT);
  strip.begin();  //Startet den Ring
  strip.show(); //Schaltet alle LEDs aus
  strip.clear();
  strip.setBrightness(25);
  Serial.begin(9600);

}

void loop() {
Lesen();
//Serial.println("Zustand1");
//Serial.println(Zustand1);
//Serial.println("Zustand2");
//Serial.println(Zustand2);
//Serial.println("Uhrzeigersinn");
//Serial.println(Uhrzeigersinn);
//Serial.println();
//Serial.println("***");
}
