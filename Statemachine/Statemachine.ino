const int LedRot=6;
const int LedGrun=7;
const int Schalter=11;


//Zustände definieren
int Zustand=0;
int Zustand0=0;
const int Z_Rot=1;
const int Z_Grun=2;
const int Z_Beide=3;
const int Z_Start=4;

//Schalter Wert
bool kurz=false;
bool lange=false;

//Überprüfung des Schalters
void proof(){
  if(digitalRead(Schalter)==HIGH){        //Wenn er gedrückt wird, wird der Wert für kurz gedrückt true
    kurz=true;
    delay(300);                           // 300 ms warten für Entprellung
    if(digitalRead(Schalter)==HIGH){      //Wenn er nach diesen 300 ms noch gedrückt ist, warten bis auf 2 s
      delay(1600);
      if(digitalRead(Schalter==HIGH)){    //Wenn er nach diesen 2s noch gedrückt ist wird lang gedrückt true
        lange=true;
      }
      else{                               //Sonst ist lang gedrückt false
        lange=false;
      }
    }
  }
}

// Zustände definieren

//Rot leuchtet (1) Z_Rot
void Rot(){
  digitalWrite(LedRot,HIGH);
  digitalWrite(LedGrun,LOW);
  Zustand=Z_Rot;
  kurz=false;
}

//Grün leuchtet (2) Z_Grun
void Grun(){
  digitalWrite(LedGrun,HIGH);
  digitalWrite(LedRot,LOW);
  Zustand=Z_Grun;
  kurz=false;
}

//Beide leuchten (3) Z_Beide
void Beide(){
  Zustand0=Zustand;
  digitalWrite(LedRot,HIGH);
  digitalWrite(LedGrun,HIGH);
  delay(5000);
  Zustand=Z_Start;
  lange=false;
  kurz=false;
}

// Verarbeitung der Überprüfung (Zustände auslösen)
void Verarbeitung(){
  if(Zustand==Z_Start){                             //Ist er im Startzustand, auf letzten gespeicherten Zustand wechseln
    if(Zustand0==Z_Rot){Rot();}
    else{Grun();}
  }
  if(lange==true){Beide();}                         //Ist lange gedrückt worden, Funktion Beide aktivieren
  if(kurz==true){                                   
    switch(Zustand){                                //Ist kurz gedrückt worden, Zustand wechseln
      case Z_Rot: Grun(); break;
      case Z_Grun: Rot(); break;
     }
   }
}


void setup() {
  // pins vorbereiten
  pinMode(LedGrun,OUTPUT);
  pinMode(LedRot,OUTPUT);
  pinMode(Schalter,INPUT);

  // Ausgangszustand (alle aus)
  digitalWrite(LedRot,LOW);
  digitalWrite(LedGrun,LOW);

  //Seriell
  Serial.begin(9600);

  kurz = false;
  lange = false;
  
  Zustand=Z_Start;                                    //In Startzustand Setzen
  
}

void loop() {
  proof();                                           // Schalter Überprüfen (Funktion) +Zustände ausgeben in Seriell
  Serial.println(Zustand);
  Serial.println(kurz);
  Serial.println(lange);
  Serial.println("...");
  
  Verarbeitung();                                    //Ergebnis der Schalter verarbeiten (Funktion) +Zustände ausgeben
  Serial.println(Zustand);
  Serial.println(kurz);
  Serial.println(lange);
  Serial.println("___");
  
}
