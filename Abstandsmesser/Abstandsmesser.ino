#include <Adafruit_LiquidCrystal.h>
//Kommentar
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
Adafruit_LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Abstandssensor
int trigger=7;
int echo=6;
unsigned long dauer=0;
float entfernung=0;

//Knopf
int knopf=8;
bool gedruckt=false;

//Array für Raum
float Raum[3];
int a=0;

void messen(){
  digitalWrite(trigger, LOW); //Hier nimmt man die Spannung für kurze Zeit vom Trigger-Pin, damit man später beim senden des Trigger-Signals ein rauschfreies Signal hat.
  delay(5); //Dauer: 5 Millisekunden
  digitalWrite(trigger, HIGH); //Jetzt sendet man eine Ultraschallwelle los.
  delay(10); //Dieser „Ton“ erklingt für 10 Millisekunden.
  digitalWrite(trigger, LOW);//Dann wird der „Ton“ abgeschaltet.
  dauer = pulseIn(echo, HIGH); //Mit dem Befehl „pulseIn“ zählt der Mikrokontroller die Zeit in Mikrosekunden, bis der Schall zum Ultraschallsensor zurückkehrt.
  entfernung = (dauer/2) * 0.03432; //Nun berechnet man die Entfernung in Zentimetern. Man teilt zunächst die Zeit durch zwei (Weil man ja nur eine Strecke berechnen möchte und nicht die Strecke hin- und zurück). Den Wert multipliziert man mit der Schallgeschwindigkeit in der Einheit Zentimeter/Mikrosekunde und erhält dann den Wert in Zentimetern.
  if (entfernung >= 500 || entfernung <= 0) //Wenn die gemessene Entfernung über 500cm oder unter 0cm liegt,…
  {
    lcd.setCursor(0,0);
    lcd.print("ERROR           ");
  }
  else{
    lcd.setCursor(0,0);
    lcd.print("Distanz ");
    lcd.print(entfernung,1);
    lcd.print(" cm ");
  }
}


void setup() {
  lcd.begin(16,2);
  pinMode(trigger,OUTPUT);
  pinMode(echo, INPUT);
  Serial.begin(9600);
}

void loop() {
  while(a<3){
    Serial.print(digitalRead(knopf));
    if(gedruckt == false){
     if(digitalRead(knopf)==HIGH){
      messen();
      Raum[a]=entfernung;
      a++;
      gedruckt=true;
     }
    }
    else{
      if(digitalRead(knopf)==LOW){
        gedruckt=false;
      }
    }
  }
 lcd.setCursor(0,0);
 lcd.print(Raum[0],0);
 lcd.print("x");
 lcd.print(Raum[1],0);
 lcd.print("x");
 lcd.print(Raum[2],0);
 lcd.print(" cm       ");
 lcd.setCursor(0,1);
 lcd.print("Vol: ");
 float vol=(Raum[0]*Raum[1]*Raum[2])/(1000000);
 lcd.print(vol,2);
 lcd.print("m3        ");
 a=0;
  
}
