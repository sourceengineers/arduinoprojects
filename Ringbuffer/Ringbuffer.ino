//Ringbuffer definieren
const int Ringbuffer_Size = 10; //Grösse des Buffers eingeben
int Ringbuffer[Ringbuffer_Size];

int Startadresse = &Ringbuffer[0];
int Endadresse = &Ringbuffer[Ringbuffer_Size-1];

//Schreib- und Lesepointer definiren
int *Schreibpointer, *Lesepointer;

//Lesefunktion (Aus serial) Gibt einen Wert aus zwischen 0 und 9 (z.B. 555 wird 5, 5, 5)
int Wert;

int Lesen(){
  Serial.println("Gib einen Wert zwischen 0 und 9 zum Speichern ein oder gib L zum ausgeben ein Bestaetige mit [ctrl]+[Enter] ");
    while(true){
      if(Serial.available() > 0){
        Wert = Serial.read();
        if(Wert <= 57 && Wert >= 48){
          return Wert-48;
        }
        else if(Wert == 108 || Wert == 76){
          return -1;
        }
      }
    }
}

//Variable für Verarbeitung
int Eingabe;

//Zähler für Schreibzyklus und Lesezyklus
int schreibcounter = 0, lesecounter = 0;

//Variable für die Ausgabe
int Ausgabe;


void setup() {
  Serial.begin(9600);
  //Pointer auf Startadresse des Arrays zeigen lassen
  Schreibpointer = Ringbuffer;
  Lesepointer = &Ringbuffer[0];
}

void loop() {
  if(Schreibpointer != 0 && Lesepointer != 0){  //Überprüfen, ob es keine Null-Poiter sind
    Eingabe = Lesen();
    if(Eingabe >= 0){                           //Eingabe Im Buffer speichern
      if(schreibcounter == (lesecounter + Ringbuffer_Size)){           //Schauen ob der zu beschreibende Pin schon ausgelesen wurde (Verhindert Datenverlust)
        Serial.println("Warnung: Keine Ueberschreibung, Dieser Wert wurde noch nicht ausgegeben! ");
      }
      else{                                     //Speichern des eingegebenen Werts im Buffer
        *Schreibpointer = Eingabe;
        schreibcounter++;
        Serial.print("Eingabe: ");
        Serial.println(Eingabe);
        if(Schreibpointer < Endadresse){        //Schreibointer auf nächstes Element (Adresse) im Buffer richten
          Schreibpointer = Schreibpointer + 1;
        }
        else{
          Schreibpointer = Startadresse;
        }
      }
    }
    else if(Eingabe == -1){                     //Wenn die Eingabe L/l ist wird eine Variable ausgegeben
        Serial.print("Gibt ");
        Serial.print(schreibcounter - lesecounter);
        Serial.println(" Werte aus:");
      while(lesecounter < schreibcounter){
        Ausgabe = *Lesepointer;                 //Schauen, ob die auszugebende Variable schon beschrieben wurde
        lesecounter++;
        Serial.println(Ausgabe);
        if(Lesepointer < Endadresse){           //Leseointer auf nächste Adresse
          Lesepointer = Lesepointer + 1;
        }
        else{
          Lesepointer = Startadresse;
        }
      }
    }

    //Variante, eine Variable ausgeben:

    // else if(Eingabe == -1){                     //Wenn die Eingabe L/l ist wird eine Variable ausgegeben
    //   if(lesecounter < schreibcounter){
    //     Ausgabe = *Lesepointer;                 //Schauen, ob die auszugebende Variable schon beschrieben wurde
    //     lesecounter++;
    //     Serial.print("Der Wert ist: ");         // Ausgabe der Variable
    //     Serial.println(Ausgabe);
    //     if(Lesepointer < Endadresse){           //Leseointer auf nächste Adresse
    //       Lesepointer = Lesepointer + 1;
    //     }
    //     else{
    //       Lesepointer = Startadresse;
    //     }
    //   }
    //   else{
    //     Serial.println("Warnung: Dieser Wert wurde noch nicht eingegeben! ");
    //   }
    // }

  }
  else{
    Serial.println("Error");
  }

  Serial.println();
  Serial.println();

//Debug Teil

  // Serial.println("_____");

  // for(int i=0; i < 10; i++){
  //   Serial.println(Ringbuffer[i]);
  // }

  // Serial.println("_____");

  // int a = Schreibpointer, b = Lesepointer;

  // Serial.println(a);
  // Serial.println(b);

  // Serial.println("_____");

  // Serial.println(s);
  // Serial.println(l);

  // Serial.println("******");
}