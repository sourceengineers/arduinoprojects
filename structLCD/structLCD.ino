#include <Adafruit_LiquidCrystal.h>

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
Adafruit_LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

char x;

struct Employee
{
  char name[50];
  int age;
  int salary;
};

typedef Employee LinusStruktur;

LinusStruktur a = {"Rolf", 25, 3200};
LinusStruktur b = {"Jana", 38, 4500};
LinusStruktur c = {"Leah", 41, 8600};
LinusStruktur d = {"Leon", 22, 5900};


char buffer [100];
char buffer2 [100];
int n;


void setup() {
  lcd.begin(16,2);
  Serial.begin(9600);

  sprintf (buffer, "Name: %s, Age: %d, Salary: %d ", a.name, a.age, a.salary);
  sprintf (buffer2,"Name: %s, Age: %d, Salary: %d ", b.name, b.age, b.salary);
  lcd.print(buffer);
  lcd.setCursor(0, 1);
  lcd.print(buffer2);
  for(int i=0;i<17;i++){
    delay(1000);
    lcd.scrollDisplayLeft();
  }
  delay(1000);
  for(int i=0;i<17;i++){
    lcd.scrollDisplayRight();
  }
  sprintf (buffer, "Name: %s, Age: %d, Salary: %d ", c.name, c.age, c.salary);
  sprintf (buffer2,"Name: %s, Age: %d, Salary: %d ", d.name, d.age, d.salary);
  lcd.setCursor(0, 0);
  lcd.print(buffer);
  lcd.setCursor(0, 1);
  lcd.print(buffer2);
  for(int i=0;i<17;i++){
    delay(1000);
    lcd.scrollDisplayLeft();
  }  
}

void loop() {

}
