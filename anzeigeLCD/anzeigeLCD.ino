#include <Adafruit_LiquidCrystal.h>

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
Adafruit_LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

unsigned long t=0;
unsigned long t0=0;
int ms=500;

void setup() {
  lcd.begin(16, 2);
  lcd.print("#Sourceengineers co Arduino");
  lcd.setCursor(0, 1);
  lcd.print("Linus Crugnola");
}

void loop() {

t=millis();
if(t-t0>=ms){
  lcd.scrollDisplayLeft();
  t0=t;
  }
}
