//Struct für Werte vorbereiten
struct _Ergebnisse {
  float R;  //R=U/I
  float P;  //P=UI
};
//Struct als Typ definieren
typedef _Ergebnisse Ergebnisse;

//Pointer auf Struct
Ergebnisse *PointerErgebnisse;

//Variablen für Eingabewerte
float U, I;

//Pointer auf U und I
float *PointerU;
float *PointerI;

//Ergebnis im Struct definieren
Ergebnisse Ergebnis;
bool resultValid;

//Letzte Ergebnisse
float ErgebnisR0, ErgebnisP0;

//Funktion zur Ausgabe des bool Werts
bool Calc(float* U, float* I, Ergebnisse* ergebnis) //Pointer die eingegeben werden
{
  if (U != 0 && I != 0) //Schaut ob Adressen der Pointer 0 sind
  {
    if (*I != 0.0)  //Verhindert Division durch 0
    {
      ergebnis->R = *U / *I;  //Widerstand rechnen
      ergebnis->P = (*U) * (*I);  //Leistung rechnen
    }
    else
    {
      return false;
    }
    return true;
  }
  else
  {
    return false;
  }
}


void setup() {
  Serial.begin(9600);

  resultValid = false;
  //Pointer auf Adresse der Eingabe zeigen lassen
  PointerU = &U;
  PointerI = &I;
  PointerErgebnisse = &Ergebnis;

  U=10;
  I=2;
}

void loop() {

  resultValid = Calc(PointerU, PointerI, PointerErgebnisse);
  if( resultValid == 1 && Ergebnis.R != ErgebnisR0 && Ergebnis.P != ErgebnisP0){
  Serial.println("result valid");
  Serial.print("R = ");
  Serial.println(Ergebnis.R);
  //Serial.println(ErgebnisR0);
  Serial.print("P = ");
  Serial.println(Ergebnis.P);
  //Serial.println(ErgebnisP0);
  ErgebnisR0 = Ergebnis.R;
  ErgebnisP0 = Ergebnis.P;
  }
  else if(resultValid==0){
  Serial.println("result invalid");
  }
  else{}
  delay(1000);

}