
//Beisielstruct definieren
struct Beispiel{
  int a;
  float b;
  char c;
  bool d;
};

Beispiel a ={124, 12.5, "l", true};
Beispiel b ={1, 247.221, "K", false};
Beispiel c ={2377, 3.14192, "Linus", true};
Beispiel d ={55, 4.383, "Bein", false};
Beispiel e ={1, 3.5, "ff", false};

int A[19];
int k;

void setup() {
  Serial.begin(9600);
  delay(2000);

  A[0]=&a.a;
  Serial.println(A[0]);
  A[1]=&a.b;
  Serial.println(A[1]);
  A[2]=&a.c;
  Serial.println(A[2]);
  A[3]=&a.d;
  Serial.println(A[3]);
  A[4]=&b.a;
  Serial.println(A[4]);
  A[5]=&b.b;
  Serial.println(A[5]);
  A[6]=&b.c;
  Serial.println(A[6]);
  A[7]=&b.d;
  Serial.println(A[7]);
  A[8]=&c.a;
  Serial.println(A[8]);
  A[9]=&c.b;
  Serial.println(A[9]);
  A[10]=&c.c;
  Serial.println(A[10]);
  A[11]=&c.d;
  Serial.println(A[11]);
  A[12]=&d.a;
  Serial.println(A[12]);  
  A[13]=&d.b;
  Serial.println(A[13]);  
  A[14]=&d.c;
  Serial.println(A[14]);  
  A[15]=&d.d;
  Serial.println(A[15]);
  A[16]=&d.d;
  Serial.println(A[16]);
  A[17]=&d.d;
  Serial.println(A[17]);
  A[18]=&d.d;
  Serial.println(A[18]);
  A[19]=&d.d;
  Serial.println(A[19]);





  k=sizeof(Beispiel);
  Serial.print(k);


}

void loop() {}

